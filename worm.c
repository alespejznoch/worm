/**
 * Copyright (c) 2018 Ales Pejznoch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 */

/**
 * Game worm made as an project to Intruduction to the C language in 2009.
 */

#include <stdio.h>
#include <ncurses.h>
#include <stdlib.h>
#include <time.h>

#define NASOBEK_OBTIZNOSTI 60
#define VYSKA 24    // SOURADNICE A VELIKOST
#define SIRKA 80    // HERNIHO POLE
#define POZX   0    // LZE MENIT
#define POZY   0

int xx,yy;

WINDOW *terminal;
WINDOW *popup;

////////////////////////////////////////////////////
/////// implementace seznamu - souradnice cerva
// struktury
typedef struct clanek_ {
  int x,y;
  struct clanek_ *dalsi;
} clanek;

typedef struct {
  clanek *prvni;
} seznam;

//inicializace
seznam* init (seznam *s) {
  s = (seznam*) malloc (sizeof(seznam));
  if (!s)
    return NULL;
  s->prvni = NULL;
  return s;
}

// pridani prvku na zacatek seznamu
int insert (seznam *s, int x, int y) {
  clanek *n;
  clanek **z;

  if ( (n = (clanek*) malloc(sizeof(clanek))) == NULL) {
    return 0;
  }

  n->x = x;
  n->y = y;

  z = &(s->prvni); // ukazatel na prvni prvek seznamu
  n->dalsi = *z;   // posun
  *z = n;          // vlozime novy prvek

  return 1;
}

// smazani posledniho prvku
void fetch (seznam *s) {
  clanek *p;
  clanek **z;
  
  z = &(s->prvni);
  
  while (*z && (*z)->dalsi)
    z = &((*z)->dalsi);
    
  p = *z;
  *z = (*z)->dalsi;
  mvwaddch(terminal, p->y, p->x,' ');
  
  free(p);
}

// vypis
void print (seznam *s) {
  clanek *p = s->prvni;

  while (p) {
    mvwaddch(terminal, p->y, p->x, ACS_DIAMOND);
    p = p->dalsi;
  }
}
// konec implementace seznamu
/////////////////////////////////////////////

void Cistka();  // reset obrazovky
void Popup();  //inicializace popup okna
void ZrusPopup();  // ukonceni popup okna

/////////////////////////////////////////////
// start vlastni hry
void Krmeni(seznam*,int);
  bool OverBod(seznam*,int,int);

int Start(int obtiznost) {
// inicializace
  Cistka();
  seznam *cerv = NULL;
  cerv = init(cerv);
  if (!cerv) return 0; 
  short int papu = 1; short int rust = 0;
  if ( !insert(cerv, 1, 1) ||
       !insert(cerv, 3, 1) ||
       !insert(cerv, 5, 1) ||
       !insert(cerv, 7, 1) ||
       !insert(cerv, 9, 1) ) return 0;
       
  print(cerv);
  Krmeni(cerv,papu);
  clanek *p = cerv->prvni;

//pohyb cerva  
  wtimeout(terminal, 0);
  short int hra = 1; short int klavesa;
  short int smer = 4;  // 1=dolu 2=nahoru 3=doleva 4=doprava 
  while (1==hra) {
    p = cerv->prvni;
  
    klavesa = wgetch(terminal);
    switch (klavesa) {
      case KEY_DOWN: if (smer!=2) smer=1; break;
      case KEY_UP: if (smer!=1) smer=2; break;
      case KEY_LEFT: if (smer!=4) smer=3; break;
      case KEY_RIGHT: if (smer!=3) smer=4; break;
      case 'Q':
      case 'q': 
          Popup();
          mvwaddstr(popup, 1, 3, "              GAME OVER");
          mvwaddstr(popup, 4, 3, "           Stisk klavesy 'Q'   ");
          mvwaddstr(popup, 7, 3, "                      pokracujte lib. klavesou..."); 
          ZrusPopup();
          
          hra=0; break;
      case 'P':
      case 'p': 
          Popup();
          mvwaddstr(popup, 1, 3, "              Hra pozastavena");
          mvwaddstr(popup, 7, 3, "                      pokracujte lib. klavesou..."); 
          ZrusPopup();
          wrefresh(terminal);
          break;
    }
    
    
    

    switch (smer) {
      case 1: if (!insert(cerv, cerv->prvni->x, cerv->prvni->y+1)) return 0; break;
      case 2: if (!insert(cerv, cerv->prvni->x, cerv->prvni->y-1)) return 0; break;
      case 3: if (!insert(cerv, cerv->prvni->x-2, cerv->prvni->y)) return 0; break;
      case 4: if (!insert(cerv, cerv->prvni->x+2, cerv->prvni->y)) return 0; break;
    }
    
    if ( ((cerv->prvni->x)==xx) && ((cerv->prvni->y)==yy) ) {
      if (papu==0) {
        hra=0;
        Popup();
        mvwaddstr(popup, 1, 3, "--------------------------------------------");
        mvwaddstr(popup, 2, 3, "    HURAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA!");
        mvwaddstr(popup, 3, 3, "--------------------------------------------");
        mvwaddstr(popup, 5, 3, "  Provedl jste cervika vsemi nastrahami.");
        mvwaddstr(popup, 6, 3, "     Jste absolutnim vitezem teto hry.");
        
        mvwaddstr(popup, 7, 3, "                      pokracujte lib. klavesou..."); 
        ZrusPopup();
      }
      rust = rust + (papu * 2);
      papu++;
      if (papu==10) papu=0;
      Krmeni(cerv,papu);    
    }
    
    // naraz do sebe
    while (p) {
      if ( ((cerv->prvni->x)==(p->x)) && ((cerv->prvni->y)==(p->y)) ) {
        hra=0;
        Popup();
        mvwaddstr(popup, 1, 3, "              GAME OVER");
        mvwaddstr(popup, 4, 3, "    Narazil jste cervikem do vlastniho tela.");
        mvwaddstr(popup, 7, 3, "                      pokracujte lib. klavesou..."); 
        ZrusPopup();
        
        
        break;
      }
      p = p->dalsi;
    }
    
    
    // naraz do zdi
    if ( ((cerv->prvni->x)==-1) || 
         ((cerv->prvni->x)==SIRKA-1) ||
         ((cerv->prvni->y)==0)  ||
         ((cerv->prvni->y)==VYSKA-1) ) {
         
           hra=0;
           Popup();
           mvwaddstr(popup, 1, 3, "              GAME OVER");
           mvwaddstr(popup, 4, 3, "       Narazil jste cervikem do steny");
           mvwaddstr(popup, 7, 3, "                      pokracujte lib. klavesou..."); 
           ZrusPopup();
    
    }

    flushinp();
    if (1==hra) {
      if (rust>0) rust--; else fetch(cerv);
      print(cerv);
      wmove(terminal, VYSKA-1, SIRKA-1);
      wrefresh(terminal);
      delay_output(obtiznost*NASOBEK_OBTIZNOSTI);
    }  
  }
  return 1;
}


// Zobrazeni papu
void Krmeni(seznam *s, int p) {
int ss,vv;
ss = SIRKA/2-1;
vv = VYSKA-2;

  xx = rand() % ss + 1;
  xx = (xx*2)-1;
  yy = rand() % vv + 1;
  
  while ( !OverBod(s, xx, yy) ) {
    xx = rand() % 39 + 1;
    xx = (xx*2)-1;
    yy = rand() % 22 + 1;
  }
  
  mvwaddch(terminal, yy, xx, '0'+p);

}

// zjisteni volneho mista pro krmeni
bool OverBod(seznam *s, int x, int y) {
  clanek *p;
  
  p = s->prvni;
  
  while (p) {
    if ( (x==(p->x)) && (y==(p->y)) ) return 0;
    p = p->dalsi;
  }
    
  return 1;
}
// konec hry
//////////////////////////////////////////


void Popup() {
    popup = subwin(terminal, VYSKA/3+1, SIRKA/2+20, POZY+VYSKA/3, POZX+SIRKA/5);
    wclear(popup);
    wborder(popup, ACS_VLINE, ACS_VLINE, '*', ACS_HLINE, 
                    ACS_ULCORNER, ACS_URCORNER, ACS_LLCORNER, ACS_LRCORNER);
    keypad(popup, TRUE);
    touchwin(terminal); 
    wrefresh(popup);
}

void ZrusPopup() {
  short int klavesa;
  do
    klavesa = wgetch(popup);
    while ( (klavesa==KEY_DOWN) || 
            (klavesa==KEY_UP)   ||
            (klavesa==KEY_LEFT) ||
            (klavesa==KEY_RIGHT) );
  wclear(popup);
  delwin(popup);
  popup = NULL;
  wrefresh(terminal);
}

void Cistka() {
  wclear(terminal);
  wborder(terminal, ACS_VLINE, ACS_VLINE, ACS_HLINE, ACS_HLINE, 
                    ACS_ULCORNER, ACS_URCORNER, ACS_LLCORNER, ACS_LRCORNER);
}  

int main (int argc, char *argv[]) { 
  if (argc>1) {
    printf("Hra CERVIK se spousti bez parametru.\n\n");
    return 2;
  }

  srand(time(NULL));
  initscr(); noecho(); keypad(stdscr, 1); // inicializace
  refresh();
  
  terminal = newwin(VYSKA, SIRKA, POZY, POZX);
  keypad(terminal, TRUE);
  
  // menu
  int cervik = 1;
  int stredx = SIRKA/2-10;
  int stredy = VYSKA/2-4;
  
  Cistka();
  mvwaddstr(terminal, stredy-3, stredx-4, "*** HRA CERVIK ***");
  wrefresh(terminal);
  
  if (!curs_set(0)) {
     Popup();
     mvwaddstr(popup, 1, 3, "              Upozorneni");
     mvwaddstr(popup, 3, 3, "     Vas terminal neumi zneviditelnit kurzor.");
     mvwaddstr(popup, 4, 3, "Program bude pracovat, ale behem hry uvidite kurzor");
     mvwaddstr(popup, 5, 3, "        v pravem dolnim rohu obrazovky.");
     mvwaddstr(popup, 7, 3, "                      pokracujte lib. klavesou...");
     ZrusPopup();
  }
  

  
  int obnov = 1;
  int submenu = 1;
  int obtiznost=2;  
  int volba = 0; 
  short int klavesa;
  wtimeout(terminal, 0);
  
////////////////////////////////////////////////////
///////////// MENU ////////////////////////////////  
  while (1==cervik) {    // Cyklus menu
    submenu = 1;
    if (1==obnov) {
      mvwaddstr(terminal, stredy-3, stredx-4, "*** HRA CERVIK ***");
      mvwaddstr(terminal, stredy  , stredx, "START HRY");
      switch (obtiznost) {
        case 1: mvwaddstr(terminal, stredy+1, stredx, "ZVOLIT OBTIZNOST (TEZKA)"); break;
        case 2: mvwaddstr(terminal, stredy+1, stredx, "ZVOLIT OBTIZNOST (STREDNI)"); break;
        case 3: mvwaddstr(terminal, stredy+1, stredx, "ZVOLIT OBTIZNOST (LEHKA)"); break;
      }      
      mvwaddstr(terminal, stredy+2, stredx, "NAPOVEDA");
      mvwaddstr(terminal, stredy+3, stredx, "KONEC");
  
      mvwaddstr(terminal, stredy, stredx-3, "->");  
      obnov=0;
    }
  
    wmove(terminal, VYSKA-1, SIRKA-1);
    wrefresh(terminal);
    klavesa = wgetch(terminal);
    
    mvwaddstr(terminal, stredy  , stredx-3, "  ");
    mvwaddstr(terminal, stredy+1, stredx-3, "  ");
    mvwaddstr(terminal, stredy+2, stredx-3, "  ");
    mvwaddstr(terminal, stredy+3, stredx-3, "  ");
    
    
    switch (klavesa) {
      case KEY_DOWN: volba++; break;
      case KEY_UP: volba--; break;
      case 'Q':
      case 'q': cervik=0; break;
      case '\n':
      case KEY_ENTER:
        switch (volba) {
          case 0: obnov=1; if (!Start(obtiznost)) {
                            delwin(terminal);
                            endwin();
                            printf("Nedostatek pameti.\n");
                            return 1;     
               
               
                            }
                Cistka(); obnov=1; break;
          case 1:
              obnov=1;
              Popup();
              mvwaddstr(popup, 1, 3, "              Zvolte obtiznost hry");
                mvwaddstr(popup, 3, 3, "     tezka");
                mvwaddstr(popup, 4, 3, "     stredni");
                mvwaddstr(popup, 5, 3, "     lehka");
                mvwaddstr(popup, 2+obtiznost, 5, "->");
              while (submenu) {  // Cyklus podmenu
                wmove(popup, VYSKA/3, SIRKA/2+20-1);     
                wrefresh(popup);
                klavesa=wgetch(popup);
                
                mvwaddstr(popup, 3, 5, "  ");
                mvwaddstr(popup, 4, 5, "  ");
                mvwaddstr(popup, 5, 5, "  ");               
                
                
                switch (klavesa) {
                  case KEY_DOWN: obtiznost++; break;
                  case KEY_UP: obtiznost--; break;
                  case '\n':
                  case KEY_ENTER:
                      submenu=0;
                   break;
                }
              
              
                if (obtiznost<1) obtiznost=3;
                if (obtiznost>3) obtiznost=1;
    
                mvwaddstr(popup, 2+obtiznost, 5, "->");
                
              }
              
              wclear(popup);
              delwin(popup);
              popup = NULL;  
            break;
            
          case 2: 
              obnov=1;
              Popup();
              mvwaddstr(popup, 1, 3, "              Napoveda");
              mvwaddstr(popup, 3, 3, "     Cervik se ovlada pomoci sipek, jeho ukolem");
              mvwaddstr(popup, 4, 3, " je sezrat vsechny cisla. Nesmi do niceho narazit.");
              mvwaddstr(popup, 5, 3, "     klavesy:  'P' PAUZA, 'Q' KONEC");
              mvwaddstr(popup, 7, 3, "                      pokracujte lib. klavesou...");
              ZrusPopup();  
            break;
          case 3: cervik=0; break;       
        
        }
        
        
        break;
    }

    if (volba<0) volba=3;
    if (volba>3) volba=0;
    
    mvwaddstr(terminal, stredy+volba, stredx-3, "->");

  
  }
  
//////////// KONEC MENU //////////////////////
//////////////////////////////////////////////
  
  delwin(terminal);
  endwin();
  
  
  
  return 0;
}
